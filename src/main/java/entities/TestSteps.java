package entities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class TestSteps {

	
	List<Item> items;

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	public TestSteps() {
		// TODO Auto-generated constructor stub
		items = new ArrayList<>();
	}
	
}
