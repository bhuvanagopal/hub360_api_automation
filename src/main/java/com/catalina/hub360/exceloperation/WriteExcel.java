package com.catalina.hub360.exceloperation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcel {

	
	public void writeEXCEL(Map<Integer, ArrayList<String>> tableRowMap,String excelName) throws IOException {
		String path = "Excel/"+excelName+".xlsx"; // path of excel workbook
		FileInputStream fileinp = new FileInputStream(path);
		XSSFWorkbook workbook = new XSSFWorkbook(fileinp);
		
//		workbook.createSheet("a");
//		workbook.removeSheetAt(0);
		
		System.out.println("writing in excel");
			XSSFSheet spreadsheet;
			//spreadsheet = workbook.getSheetAt("a");
			spreadsheet = workbook.getSheetAt(0);
			XSSFRow row;
			
			
			Set<Integer> keyid = tableRowMap.keySet();
			int rowid = 0;

			for (Integer key : keyid) {
				row = spreadsheet.createRow(rowid++);
				ArrayList<String> excelRowList = tableRowMap.get(key);
				int cellid = 0;

				for (String str : excelRowList) {
					Cell cell = row.createCell(cellid++);
					cell.setCellValue((String) str);
				}
			}

		

		FileOutputStream out = new FileOutputStream(new File("excel/"+excelName+".xlsx"));

		workbook.write(out);
		out.close();
	}
}
