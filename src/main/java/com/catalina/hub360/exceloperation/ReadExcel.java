package com.catalina.hub360.exceloperation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel {
	
	public Workbook WorkBook;
	public Sheet Sheet;
	public Row row;
	public Cell cell;
	public DataFormatter dfTestData;

	public static ArrayList< ArrayList<String>> convertExcelIntoArrayList(String workbookName,String sheetName)
			throws EncryptedDocumentException, InvalidFormatException, IOException {

		
		String XLpath = "Excel/"+workbookName+".xlsx";
		Workbook workbook = WorkbookFactory.create(new File(XLpath));
		
		 ArrayList< ArrayList<String>> ExcelArrayList = new  ArrayList<ArrayList<String>>();

		Sheet sheet = workbook.getSheet(sheetName);
		DataFormatter dataformator = new DataFormatter();
		int key = 0;
		Iterator<Row> rowiterator = sheet.rowIterator();
		///fetch data coloumn wise
		while (rowiterator.hasNext()) {
			ArrayList<String> arr = new ArrayList<String>();
			Row row = rowiterator.next();
			Iterator<Cell> celliterator = row.cellIterator();
			while (celliterator.hasNext()) {
				Cell cell = celliterator.next();
				String cellValue = dataformator.formatCellValue(cell);
				arr.add(cellValue);
			}
			ExcelArrayList.add(key,arr);
			key++;
			
		}
		return ExcelArrayList;
	}
	
	public String  readTextFile(String fileName) throws IOException {
		String fileContent="";
		
		 File file = new File("Text/"+fileName+".txt"); 
		  
		  BufferedReader br = new BufferedReader(new FileReader(file)); 
		  
		  String st; 
		  while ((st = br.readLine()) != null) 
			  fileContent=fileContent+st;
		  
		  return fileContent;
		  } 
	public ArrayList<String> returnColoumnOfTheExcel(ArrayList< ArrayList<String>> ExcelArrayList, String searchColoumName){
		ArrayList<String> SearchColoumnValue= new ArrayList<String>();
		ArrayList<String> coloumnNameList= ExcelArrayList.get(0);
		int coloumnIndex=-1;
		
		for(int i=0;i<coloumnNameList.size();i++) {
			String coloumnName=coloumnNameList.get(i);
			
			if(coloumnName.contentEquals(searchColoumName)) {
				coloumnIndex=i;
			}
		}
		
		for(int i=1;i<ExcelArrayList.size();i++) {
			ArrayList<String> coloumnValueList=ExcelArrayList.get(i);
			SearchColoumnValue.add(coloumnValueList.get(coloumnIndex));
			
		}
		
		
		return SearchColoumnValue;
		
	}
	
	public String returnCellValueOfExcel(ArrayList< ArrayList<String>> ExcelArrayList, String searchColoumName){
		ArrayList<String> SearchColoumnValue= new ArrayList<String>();
		ArrayList<String> coloumnNameList= ExcelArrayList.get(0);
		int coloumnIndex=-1;
		String cellValue;
		
		for(int i=0;i<coloumnNameList.size();i++) {
			String coloumnName=coloumnNameList.get(i);
			
			if(coloumnName.contentEquals(searchColoumName)) {
				coloumnIndex=i;
			}
		}
		
		for(int i=1;i<ExcelArrayList.size();i++) {
			ArrayList<String> coloumnValueList=ExcelArrayList.get(i);
			SearchColoumnValue.add(coloumnValueList.get(coloumnIndex));
			
		}
		cellValue = SearchColoumnValue.toString();
		
		return cellValue;
		
	}
	
	public String getCellVal(String sheetName, int rowNum, int colNum) {

		try {

			Sheet = WorkBook.getSheet(sheetName);
			dfTestData = new DataFormatter();
			row = Sheet.getRow(rowNum);
			Cell cellVal = row.getCell(colNum);
			String excelVal = dfTestData.formatCellValue(cellVal).trim();
			return excelVal;

		} catch (NullPointerException e) {
			System.out.println(
					"There is no value in the cell, sheetname:" + sheetName + ",row:" + rowNum + ", column=" + colNum);
			return "";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return null;
		}

	}
}
