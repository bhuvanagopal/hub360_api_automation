package com.catalina.hub360.exceloperation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.catalina.hub360.exceloperation.ReadExcel;
import com.catalina.hub360.steps.ApiOperation;
import com.catalina.hub360.utils.JsonUtils;
import com.catalina.hub360.utils.NetworkUtils;
import com.catalina.hub360.utils.TestCase;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ExcelComputation {
	ApiOperation apiOperation;

	public String hitApi(ArrayList<ArrayList<String>> apiNameExcelList, String request, String endPoint, Logger log,
			TestCase testCase, Map<String, String> localValueList,String valueFile) throws IOException {
		ArrayList<String> requestList = new ArrayList<String>();
		ArrayList<String> excelColoumnName = new ArrayList<String>();
		String response = null;
		try {
			excelColoumnName = excelColoumnName(apiNameExcelList);
			ArrayList<ArrayList<String>> allColoumnValue = returnValueOfApiExcelList(excelColoumnName,
					apiNameExcelList);

			if (valueFile.contentEquals("LOCAL")) {
				System.out.println("Local");
				requestList = returnArayListOfRequestForLocalValues(localValueList, request);
				
			} else {
				System.out.println("Global");
				requestList = returnArrayListOfRequestForEachValue(excelColoumnName, allColoumnValue, request);
			}
		} catch (Exception e) {
			requestList.add(request);
		}
		int responseLength = 0;
		for (String postBody : requestList) {
			
			testCase.setRequest(postBody);
			apiOperation = new ApiOperation();
			
			response = apiOperation.POSTRequest(endPoint, postBody, "General", log, testCase);
			
			log.info("//////////////  RESPONSE RECIEVED /////////");
			log.info(response);
			log.info("/////////////////////////////////////////////");
			
			JsonElement element = JsonUtils.convertToJsonElement(response);
			JsonObject arr = element.getAsJsonObject().get("data").getAsJsonObject();
			responseLength = arr.size();
			System.out.println(responseLength);
			}
		if(responseLength>0)
		{
		return response;
		}
		else
		{
			return null;
		}

	}

	public ArrayList<String> excelColoumnName(ArrayList<ArrayList<String>> apiNameExcelList) {
		ArrayList<String> excelColoumnName = new ArrayList<String>();
		excelColoumnName = apiNameExcelList.get(0);
		return excelColoumnName;

	}

	public ArrayList<ArrayList<String>> returnValueOfApiExcelList(ArrayList<String> excelColoumnName,
			ArrayList<ArrayList<String>> apiNameExcelList) {
		ArrayList<ArrayList<String>> apiExcelValueList = new ArrayList<ArrayList<String>>();
		ReadExcel readExcel = new ReadExcel();

		for (int i = 0; i < excelColoumnName.size(); i++) {
			String coloumnName = excelColoumnName.get(i);
			ArrayList<String> coloumnList = readExcel.returnColoumnOfTheExcel(apiNameExcelList, coloumnName);
			apiExcelValueList.add(coloumnList);
		}
		return apiExcelValueList;
	}

	public ArrayList<String> returnArrayListOfRequestForEachValue(ArrayList<String> excelColoumnName,
			ArrayList<ArrayList<String>> allColoumnValue, String request) {
		ArrayList<String> requestList = new ArrayList<String>();

		for (int i = 0; i < allColoumnValue.get(0).size(); i++) {
			String replaceRequest = request;
			for (int n = 0; n < excelColoumnName.size(); n++) {
				String replacement = allColoumnValue.get(n).get(i);
				String replaceValue = excelColoumnName.get(n);
				replaceRequest = replaceRequest.replaceAll(replaceValue, replacement);
			}
			requestList.add(replaceRequest);
		}

		return requestList;
	}
	
	
	public Map<String, String> returnLocalValueList(ArrayList<ArrayList<String>> mainExcelArrayList, int index){
		Map<String, String> localValueList = new HashMap<String, String>();
		ReadExcel readExcel = new ReadExcel();
		
		ArrayList<String> listIdList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "LISTID");
		ArrayList<String> campaignIdList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "CAMPAIGNID");
		ArrayList<String> startDateIdList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "STARTDATE");
		ArrayList<String> endDateIdList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "ENDDATE");
		ArrayList<String> TimeframeList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "TIMEFRAME");
		ArrayList<String> GroupIdList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "GROUPID");
		
		localValueList.put("LISTID", listIdList.get(index));
		localValueList.put("CAMPAIGNID", campaignIdList.get(index));
		localValueList.put("STARTDATE", startDateIdList.get(index));
		localValueList.put("ENDDATE", endDateIdList.get(index));
		localValueList.put("TIMEFRAME", TimeframeList.get(index));
		localValueList.put("GROUPID", GroupIdList.get(index));
		return localValueList;
		
	}
	
	public ArrayList<String> returnArayListOfRequestForLocalValues( Map<String, String> localValueList , String request){
		ArrayList<String> requestList = new ArrayList<String>();
		Set<String> keySet=localValueList.keySet();
		String replaceRequest = request;
		for(String key : keySet) {
			
			replaceRequest=replaceRequest.replaceAll(key, localValueList.get(key));
			System.out.println(localValueList.get(key));
			
		}
		System.out.println(replaceRequest);
		requestList.add(replaceRequest);
		
		return requestList;
		
	}
}
