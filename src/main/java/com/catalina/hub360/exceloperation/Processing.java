package com.catalina.hub360.exceloperation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;

import com.catalina.hub360.utils.TestCase;

public class Processing {

	public void excelProcessing(String endPoint, Logger log)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		ReadExcel readExcel = new ReadExcel();
		ExcelComputation excelComputation = new ExcelComputation();

		ArrayList<ArrayList<String>> mainExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "main");


		ArrayList<String> clientNameList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client name");
		System.out.println("Clientnamelist "+clientNameList);
		ArrayList<String> clientIdList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client id");
		ArrayList<String> APIsList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "APIs");
		ArrayList<String> valueFileList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "File");

		for(int i=0; i<clientNameList.size();i++ ) {
			TestCase testCase = new TestCase();

			String clientName=clientNameList.get(i);
			System.out.println(clientName);
			String clientId=clientIdList.get(i);
			String apiType = APIsList.get(i);	
			String valueFile = valueFileList.get(i);

			testCase.setClientName(clientName);
			testCase.setClientId(clientId);

			Map<String, String> localValueList = excelComputation.returnLocalValueList(mainExcelArrayList, i);
			log.info(" Client is "+clientName);

			String[] typeArray = apiType.split(",");

			for (String type : typeArray) {

				log.info(" API type "+type);
				testCase.setApiType(type);

				ArrayList<ArrayList<String>> ExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", type);
				ArrayList<String> APINameList = readExcel.returnColoumnOfTheExcel(ExcelArrayList, "API Name");
				ArrayList<String> RequestList = readExcel.returnColoumnOfTheExcel(ExcelArrayList, "Request");

				for (int j = 0; j < APINameList.size(); j++) {
					String apiName = APINameList.get(j);
					String request = RequestList.get(j);

					log.info("API NAME "+apiName);
					testCase.setApiName(apiName);

					ArrayList<ArrayList<String>> apiNameExcelList = readExcel.convertExcelIntoArrayList(type,apiName);
					String response = excelComputation.hitApi(apiNameExcelList, request, endPoint,log,testCase,localValueList,valueFile);
					if(response == null)
					{
						System.out.println("Returning No data for the client "+ apiName);
						Assert.fail();
					}
				}
			}
		}
	}

}