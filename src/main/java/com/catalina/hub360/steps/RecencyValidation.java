package com.catalina.hub360.steps;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.catalina.hub360.utils.ExtentConfig;
import com.catalina.hub360.utils.ReadPropertyFile;



public class RecencyValidation extends ExtentConfig{
	private static final Logger log = Logger.getLogger(RecencyValidation.class);
	Verification verification;

	
	@Test
	public void Validation_RecencyValidation() throws Throwable {
		test = report.startTest("Validation_RecencyValidation");
		ReadPropertyFile readPropertyFile = new ReadPropertyFile();
		String endPoint = readPropertyFile.returnValue("EndPoint");
		try
		{
			verification = new Verification();
			verification.RecencyValidation(endPoint, log, test);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

}
