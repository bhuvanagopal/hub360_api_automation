package com.catalina.hub360.steps;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.catalina.hub360.exceloperation.Processing;
import com.catalina.hub360.utils.Computation;
import com.catalina.hub360.utils.Creation;
import com.catalina.hub360.utils.ExtentConfig;
import com.catalina.hub360.utils.ReadPropertyFile;
import com.catalina.hub360.utils.Report;
import com.catalina.hub360.utils.TargetProcess;


public class Hub extends ExtentConfig {
	private static final Logger log = Logger.getLogger(Hub.class);
	Verification verification;

	private static final TargetProcess tp = new TargetProcess();
	public int testPlanRunID = 0;
	private static int testPlanId = 0;
	static String buildName = null;
	public static int buildID = -1;
	static ReadPropertyFile readPropertyFile;

	@Test
	public void hub360() throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		
	    testPlanId = Integer.parseInt(readPropertyFile.getTestPlanID());
	    buildName = readPropertyFile.getBuildName();
		System.out.println(testPlanId+" "+buildName);

		System.out.println("testPlanId:::::::::::"+testPlanId);
		System.out.println("Build Name:::::::"+buildName);	


		buildID =tp.createBuild(null, testPlanId, buildName);
		System.out.println("Build Number:::::"+tp.getBuildId());
		
		ApiOperation apiOperation = new ApiOperation();
		Processing processing = new Processing();
		Creation creation = new Creation();
		Computation composition = new Computation();
		
		readPropertyFile.configureLog4j();
		String endPoint = readPropertyFile.returnValue("EndPoint");
		apiOperation.getSessionToken(endPoint, log);
		processing.excelProcessing(endPoint, log);

		apiOperation.fetchTestCaseDetails();
		Map<String, ArrayList<Report>> reportRunDetailMap = composition.reportDataProcessing();
		test = creation.createExtentReport(test, report, reportRunDetailMap);

		
	}

	@AfterTest
	public static void writeExtentReport() throws Exception {
		report.flush();
		tp.uploadAttachment(new File("./ExtentReportResults.html"), buildID);
	}
}
