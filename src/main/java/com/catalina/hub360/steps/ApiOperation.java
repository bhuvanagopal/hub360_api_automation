package com.catalina.hub360.steps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;

import com.catalina.hub360.exceloperation.ReadExcel;
import com.catalina.hub360.exceloperation.WriteExcel;
import com.catalina.hub360.utils.Computation;
import com.catalina.hub360.utils.TestCase;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ApiOperation {


	private static String TOKEN="";
	static  ArrayList<TestCase> testCaseDetailList = new ArrayList<TestCase>();
	Map<Integer, ArrayList<String>> testCaseDetailMap = new HashMap<Integer, ArrayList<String>>();

	public void getSessionToken(String endPoint,Logger log) throws IOException {
		ApiOperation apiOperation = new ApiOperation();
		TestCase testCase = new TestCase();
		ReadExcel readExcel = new ReadExcel();

		String postBody = readExcel.readTextFile("postRequestBody");

		String reponseString = apiOperation.POSTRequest(endPoint,postBody,"sessionId",log,testCase);

		try {
			String token = apiOperation.returnSessionTokenFromResponse(reponseString);
			TOKEN=token;
			log.info("Token recieved ");
			log.info("Token :  "+token);
		}catch (Exception e) {
			log.error(" Unable to get token ");
		}
	}

	public String POSTRequest(String endPoint, String postBody, String use,Logger log,TestCase testCase) throws IOException {

		log.info("Endpoint = "+endPoint);
		log.info("Postbody = "+postBody);
		long startTime = System.currentTimeMillis();


		URL obj = new URL(endPoint);
		HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
		postConnection.setRequestMethod("POST");

		postConnection.setRequestProperty("Host","hub360-api.catalina.com");
		postConnection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0");
		postConnection.setRequestProperty("Accept","application/json, text/plain, */*");
		postConnection.setRequestProperty("Accept-Language","en-US,en;q=0.5");
		postConnection.setRequestProperty("Accept-Encoding","gzip, deflate, br");
		postConnection.setRequestProperty("Content-Type","application/json");
		postConnection.setRequestProperty("Content-Length","268");
		postConnection.setRequestProperty("Origin","https://hub360.catalina.com");
		postConnection.setRequestProperty("Connection","keep-alive");
		postConnection.setRequestProperty("Referer","https://hub360.catalina.com/login");
		postConnection.setRequestProperty("Sec-Fetch-Site", "same-site");
		postConnection.setRequestProperty("Sec-Fetch-Mode", "cors");

		if(use.equalsIgnoreCase("General")) {
			postConnection.setRequestProperty("Authorization","Bearer "+TOKEN);
		}

		postConnection.setDoOutput(true);


		OutputStream os = postConnection.getOutputStream();
		os.write(postBody.getBytes());
		os.flush();
		os.close();

		int responseCode = postConnection.getResponseCode();
		String responseMessage=  postConnection.getResponseMessage();


		log.info("Response message = "+responseMessage);
		log.info("Response code = "+responseCode);

		testCase.setResponseCode(Integer.toString(responseCode));
		testCase.setResponseMessage(responseMessage);
		
		

		//success
		StringBuffer response = new StringBuffer();

		try {
			BufferedReader in = null;

			if (postConnection.getHeaderField("Content-Encoding")!=null && postConnection.getHeaderField("Content-Encoding").equals("gzip"))
			{
				in = new BufferedReader(new InputStreamReader(new GZIPInputStream(postConnection.getInputStream())));            
			} 
			else 
			{
				in = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));            
			}  

			String inputLine;
			while ((inputLine = in .readLine()) != null) {
				response.append(inputLine);
			} 
			in .close();


		}catch (Exception e) {
			response.append("failed to get response");
		}

		log.info("Response message");
		testCase.setResponse(response.toString());

		long elapsedTime = System.currentTimeMillis() - startTime;
		testCase.setResponseTime(String.valueOf(elapsedTime)+" milliseconds");
		if(use.equalsIgnoreCase("General")) {
			//System.out.println("@@@@@"+testCase);
			TestCase testCaseCopy = new TestCase();
			testCaseCopy.setClientName(testCase.getClientName());
			testCaseCopy.setClientId(testCase.getClientId());
			testCaseCopy.setListId(testCase.getListId());
			testCaseCopy.setApiType(testCase.getApiType());
			testCaseCopy.setApiName(testCase.getApiName());
			testCaseCopy.setRequest(testCase.getRequest());
			testCaseCopy.setResponse(testCase.getResponse());
			testCaseCopy.setResponseCode(testCase.getResponseCode());
			testCaseCopy.setResponseMessage(testCase.getResponseMessage());
			testCaseCopy.setResponseTime(testCase.getResponseTime());
			testCaseDetailList.add(testCaseCopy);
		}


		log.info("Response Time : "+elapsedTime+"milliseconds");
		return (response.toString());

	}

	public String returnSessionTokenFromResponse(String response) {

		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject) parser.parse(response).getAsJsonObject();
		JsonObject dataObject =object.getAsJsonObject("data").getAsJsonObject();
		JsonObject beginSessionObject =dataObject.getAsJsonObject("beginSession").getAsJsonObject();
		String sessionTokenvalue=beginSessionObject.getAsJsonPrimitive("sessionToken").getAsString();

		return sessionTokenvalue;
	}

	public void fetchTestCaseDetails() throws IOException {
		Computation computation = new Computation();
		computation.initializeTestRunDetails(testCaseDetailList);



		for(int i=0; i<testCaseDetailList.size();i++) {
			ArrayList<String> testCaseList = new ArrayList<String>();
			TestCase testCase= testCaseDetailList.get(i);


			testCaseList.add(testCase.getClientName());	
			testCaseList.add(testCase.getClientId());
			testCaseList.add(testCase.getListId());	
			testCaseList.add(testCase.getApiType());		
			testCaseList.add(testCase.getApiName());
			testCaseList.add(testCase.getRequest());
			String response=testCase.getResponse();
			if(response.length()<32766) {
				testCaseList.add(response);
			}else {
				testCaseList.add("response too long to show in excel check logs instead");

			}

			testCaseList.add(testCase.getResponseCode());
			testCaseList.add(testCase.getResponseMessage());
			testCaseList.add(testCase.getResponseTime());
			testCaseDetailMap.put(i, testCaseList);
		}
		WriteExcel 	writeExcel = new WriteExcel();
		writeExcel.writeEXCEL(testCaseDetailMap, "RunDetails");

	}

}
