package com.catalina.hub360.steps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Assert;

import com.catalina.hub360.exceloperation.ExcelComputation;
import com.catalina.hub360.exceloperation.ReadExcel;
import com.catalina.hub360.utils.ExcelUtility;
import com.catalina.hub360.utils.JsonUtils;
import com.catalina.hub360.utils.NetworkUtils;
import com.catalina.hub360.utils.ReadPropertyFile;
import com.catalina.hub360.utils.TestCase;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * rename the class file as TargetingUtil rather than verification
 * 
 * @author csauto4
 *
 */
public class Verification {

	ReadPropertyFile readPropertyFile = new ReadPropertyFile();
	ExcelComputation excelComputation = new ExcelComputation();
	ReadExcel readExcel = new ReadExcel();
	ExcelUtility excel;
	TestCase testCase = new TestCase();
	List<String> listAPI = new ArrayList<String>();
	List<String> listSolr = new ArrayList<String>();
	String response = null;
	Map<String, String> localValueList;
	String valueFile;
	String request;
	String type;
	String apiName;
	String endPointSolr;
	String campaignEndPoint;
	String tab;
	String campaignId;

	public void readExcel(String endPoint, Logger log, ExtentTest test, String tabName, int i)
	{


		ReadExcel readExcel = new ReadExcel();
		ExcelComputation excelComputation = new ExcelComputation();
		try {

			ArrayList<ArrayList<String>> mainExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "main");

			ArrayList<String> clientNameList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client name");
			ArrayList<String> clientIdList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client id");
			ArrayList<String> APIsList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "APIs");
			ArrayList<String> valueFileList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "File");

			TestCase testCase = new TestCase();

			String clientName = clientNameList.get(i);
			String clientId = clientIdList.get(i);
			String apiType = APIsList.get(i);
			this.valueFile = valueFileList.get(i);

			testCase.setClientName(clientName);
			testCase.setClientId(clientId);

			this.localValueList = excelComputation.returnLocalValueList(mainExcelArrayList, i);

			System.out.println(localValueList);
			log.info(" Client is " + clientName);

			String[] typeArray = apiType.split(",");

			for (String type : typeArray) {

				log.info(" API type " + type);
				testCase.setApiType(type);
				this.type = type;
				if (type.equalsIgnoreCase("MTA")) {
					ArrayList<ArrayList<String>> ExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel",type);
					ArrayList<String> APINameList = readExcel.returnColoumnOfTheExcel(ExcelArrayList, "API Name");
					ArrayList<String> RequestList = readExcel.returnColoumnOfTheExcel(ExcelArrayList, "Request");
					ArrayList<String> ValidationList = readExcel.returnColoumnOfTheExcel(ExcelArrayList,
							"Validation");

					for (int j = 0; j < APINameList.size(); j++) {
						this.apiName = APINameList.get(j);
						this.request = RequestList.get(j);
						String validation = ValidationList.get(j);
						log.info("API NAME " + apiName);
						testCase.setApiName(apiName);

						if (apiName.equalsIgnoreCase("CampaignData")) {

							if (validation.equalsIgnoreCase("Yes")) {
								ArrayList<ArrayList<String>> ValidationArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "Validation");
								ArrayList<String> APInameList = readExcel.returnColoumnOfTheExcel(ValidationArrayList, "API Name");
								ArrayList<String> TabList = readExcel.returnColoumnOfTheExcel(ValidationArrayList,"Tab");
								ArrayList<String> EnDPointList = readExcel.returnColoumnOfTheExcel(ValidationArrayList, "EndPoint");
								ArrayList<String> CampaignEnDPointList = readExcel.returnColoumnOfTheExcel(ValidationArrayList, "CampaignEndPoint");


								for (int l = 0; l < APInameList.size(); l++) {
									String api_Name = APInameList.get(l);
									if (api_Name.equalsIgnoreCase(apiName))
									{
										if(TabList.get(l).equals(tabName))
										{
											this.endPointSolr = EnDPointList.get(l);
											this.campaignEndPoint = CampaignEnDPointList.get(l);
											String responseSolr = NetworkUtils.getCall(campaignEndPoint);
											JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);
											JsonArray campaignSolr = elementSolr.getAsJsonObject().get("facet_counts").getAsJsonObject().get("facet_fields").getAsJsonObject().get("campaign_id").getAsJsonArray();
											for(int m =0; m <campaignSolr.size(); m++)
											{
												Boolean verification = true;
												if(Integer.parseInt(campaignSolr.get(1).getAsString())<100000)
												{
													if(campaignSolr.get(m).getAsString().length()>7)
													{
														this.campaignId  = campaignSolr.get(m).getAsString();
														if(TabList.get(l).contains("Creative"))
														{
														this.request = readExcel.readTextFile("postRequestBodyCampaignData");
														this.request = request.replace("CAMPAIGNID", campaignSolr.get(m).getAsString());
														}
													}
												}
												else
												{
													request = readExcel.readTextFile("postRequestBodyCampaignData");
													request = request.replace("CAMPAIGNID", campaignSolr.get(m).getAsString());
													ApiOperation apiOperation = new ApiOperation();
													response = apiOperation.POSTRequest(endPoint, request, "General", log, testCase);
													System.out.println("Response   "+response);
													JsonElement element = JsonUtils.convertToJsonElement(response);
													String message = element.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").getAsString();
													System.out.println("Message  "+message);
													Assert.assertEquals(message, "API_DATA_LIMIT_ERROR");
													verification = false;
												}
												Assert.assertEquals("Getting API data limit error!!!!!", true, verification);
												break;
											}
											break;
										}
									}
								}
							}
							break;
						}
					}
				}
				break;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	public void upc_DetailsValidation(String endPoint, Logger log, ExtentTest test) throws Throwable {

		ArrayList<ArrayList<String>> mainExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "main");

		ArrayList<String> clientNameList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client name");

		for (int i = 0; i < clientNameList.size(); i++) {
			try
			{
				String upc = null;

				readExcel(endPoint, log, test, "UPCDetails", i);
				ArrayList<String> requestList = excelComputation.returnArayListOfRequestForLocalValues(localValueList, endPointSolr);
				String EndPointSolr = requestList.toString().replaceAll("[\\[\\]]", "");
				System.out.println("EndPointSolr    " + EndPointSolr);
				String responseSolr = NetworkUtils.getCall(EndPointSolr);
				JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);

				ArrayList<ArrayList<String>> apiNameExcelList = readExcel.convertExcelIntoArrayList(type, apiName);
				response = excelComputation.hitApi(apiNameExcelList, request, endPoint,
						log, testCase, localValueList, valueFile);
 
				System.out.println("response   :   "+response);
				JsonElement element = JsonUtils.convertToJsonElement(response);
				JsonArray arr = element.getAsJsonObject().get("data").getAsJsonObject()
						.get("campaign").getAsJsonObject().get("upcList").getAsJsonArray();
				for(int a=0; a<arr.size(); a++)
				{
					listAPI = new ArrayList<String>();
					listSolr = new ArrayList<String>();
					System.out.println(a);
					upc = arr.get(a).getAsJsonObject().get("upc").getAsString();
					listAPI.add(arr.get(a).getAsJsonObject().get("upc").getAsString());
					listAPI.add(arr.get(a).getAsJsonObject().get("brand").getAsString());
					listAPI.add(arr.get(a).getAsJsonObject().get("description").getAsString());
					listAPI.add(arr.get(a).getAsJsonObject().get("newUpcFlag").getAsString());
					listAPI.add(arr.get(a).getAsJsonObject().get("category").getAsString());
					listAPI.add(arr.get(a).getAsJsonObject().get("productType").getAsString());
					listAPI.add(arr.get(a).getAsJsonObject().get("attribute").getAsString());
					listAPI.add(arr.get(a).getAsJsonObject().get("size").getAsString());

					System.out.println(listAPI);


					JsonArray arrSolr = elementSolr.getAsJsonObject().get("response")
							.getAsJsonObject().get("docs").getAsJsonArray();

					for(int b=0; b<arrSolr.size(); b++)
					{
						if(arrSolr.get(b).getAsJsonObject().get("upc").getAsString().contains(upc))
						{
							listSolr.add(arrSolr.get(b).getAsJsonObject().get("upc").getAsString());
							listSolr.add(arrSolr.get(b).getAsJsonObject().get("brand_descr").getAsString());
							listSolr.add(arrSolr.get(b).getAsJsonObject().get("upc_descr").getAsString());
							String flag = arrSolr.get(b).getAsJsonObject().get("new_upc_flag").getAsString();
							if(flag.contains("false"))
							{
								listSolr.add("N");
							}
							else
							{
								listSolr.add("Y");
							}
							listSolr.add(arrSolr.get(b).getAsJsonObject().get("key_cat_descr").getAsString());
							listSolr.add(arrSolr.get(b).getAsJsonObject().get("product_type").getAsString());
							listSolr.add(arrSolr.get(b).getAsJsonObject().get("flavor_scent_descr").getAsString());
							listSolr.add(arrSolr.get(b).getAsJsonObject().get("size").getAsString());
							System.out.println(listSolr);
							break;
						}
						else
						{
						}

					}
					Assert.assertEquals(listAPI, listSolr);
					test.log(LogStatus.INFO, "UPC List From API : "+ clientNameList.get(i) + "\n" + listAPI);
					test.log(LogStatus.INFO, "UPC List From Solr : " + clientNameList.get(i)+ "\n" + listSolr);
					test.log(LogStatus.PASS, "UPC Details of API and Solr are matched. Hence the Validation is Passed!! for" + clientNameList.get(i));
				}

			} 
			catch(NullPointerException ne)
			{
				System.out.println("No  data found");
				test.log(LogStatus.FAIL, "UPC Details of API and Solr Didn't matched. Hence the Validation Failed!!");
				Assert.fail("Test case Failed   :  " + ne.getMessage());
			}
			catch (AssertionError e) {
				System.out.println("Data didnt matched");
				test.log(LogStatus.INFO, "UPC List From API : " + "\n" + listAPI);
				test.log(LogStatus.INFO, "UPC List From Solr : " + "\n" + listSolr);
				test.log(LogStatus.FAIL, "UPC Details of API and Solr Didn't matched. Hence the Validation Failed!!");
				Assert.fail("Test case Failed   :  " + e.getMessage());
			}
		}
	}

	public void cartInSight_DataValidation(String endPoint, Logger log, ExtentTest test) throws Throwable {
		String response = null;
		ReadExcel readExcel = new ReadExcel();
		ExcelComputation excelComputation = new ExcelComputation();
		try {

			ArrayList<ArrayList<String>> mainExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "main");

			ArrayList<String> clientNameList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client name");
			ArrayList<String> clientIdList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client id");
			ArrayList<String> APIsList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "APIs");
			ArrayList<String> valueFileList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "File");

			for (int i = 0; i < clientNameList.size(); i++) {
				TestCase testCase = new TestCase();

				String clientName = clientNameList.get(i);
				String clientId = clientIdList.get(i);
				String apiType = APIsList.get(i);
				String valueFile = valueFileList.get(i);

				testCase.setClientName(clientName);
				testCase.setClientId(clientId);

				Map<String, String> localValueList = excelComputation.returnLocalValueList(mainExcelArrayList, i);

				System.out.println(localValueList);
				log.info(" Client is " + clientName);

				String[] typeArray = apiType.split(",");

				for (String type : typeArray) {

					log.info(" API type " + type);
					testCase.setApiType(type);
					if (type.equalsIgnoreCase("MTA")) {
						ArrayList<ArrayList<String>> ExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel",type);
						ArrayList<String> APINameList = readExcel.returnColoumnOfTheExcel(ExcelArrayList, "API Name");
						ArrayList<String> RequestList = readExcel.returnColoumnOfTheExcel(ExcelArrayList, "Request");
						ArrayList<String> ValidationList = readExcel.returnColoumnOfTheExcel(ExcelArrayList,"Validation");

						for (int j = 0; j < APINameList.size(); j++) {
							String apiName = APINameList.get(j);
							String validation = ValidationList.get(j);
							log.info("API NAME " + apiName);
							testCase.setApiName(apiName);

							if (apiName.equalsIgnoreCase("CampaignData")) {

								if (validation.equalsIgnoreCase("Yes")) {
									ArrayList<ArrayList<String>> ValidationArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "Validation");
									ArrayList<String> APInameList = readExcel.returnColoumnOfTheExcel(ValidationArrayList, "API Name");
									ArrayList<String> TabList = readExcel.returnColoumnOfTheExcel(ValidationArrayList,"Tab");
									ArrayList<String> EnDPointList = readExcel.returnColoumnOfTheExcel(ValidationArrayList, "EndPoint");

									for (int l = 0; l < APInameList.size(); l++) {
										String api_Name = APInameList.get(l);
										if (api_Name.equalsIgnoreCase(apiName)) {
											String endPointSolr = EnDPointList.get(l);
											String tab = TabList.get(l);
											System.out.println("EndPointSolr    " + endPointSolr);

											if (tab.equalsIgnoreCase("CartInSight")) {
												readPropertyFile = new ReadPropertyFile();
												String EndPoint = "http://10.165.139.210:32032/graphql";
												String Request = null;

												ArrayList<ArrayList<String>> apiNameExcelList = new ArrayList<ArrayList<String>>();
												ArrayList<String> inner = new ArrayList<String>();
												ArrayList<String> inner1 = new ArrayList<String>();
												inner.add("CAMPAIGNID");
												inner.add("STARTDATE");
												inner.add("ENDDATE");
												inner.add("NATIONAL");
												inner1.add("a9364298-8041-40cf-aa4c-c7bb719bdee8");
												inner1.add("2019-09-01");
												inner1.add("2019-09-30");
												inner1.add("national");
												apiNameExcelList.add(inner);
												apiNameExcelList.add(inner1);
												response = excelComputation.hitApi(apiNameExcelList, Request, EndPoint,
														log, testCase, localValueList, valueFile);
												JsonElement element = JsonUtils.convertToJsonElement(response);

												JsonArray arr = element.getAsJsonObject().get("data").getAsJsonObject()
														.get("campaign").getAsJsonObject().get("cartInsights")
														.getAsJsonArray();
												arr.iterator().forEachRemaining(p -> listAPI
														.add(p.getAsJsonObject().get("brand").getAsString()));
												Set<String> set = new HashSet<String>(listAPI);
												listAPI.clear();
												listAPI.addAll(set);

												String responseSolr = NetworkUtils.getCall(endPointSolr);
												JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);
												JsonArray arrSolr = elementSolr.getAsJsonObject().get("response")
														.getAsJsonObject().get("docs").getAsJsonArray();
												arrSolr.iterator().forEachRemaining(p -> listSolr
														.add(p.getAsJsonObject().get("name").getAsString()));

												Set<String> setSolr = new HashSet<String>(listSolr);
												listSolr.clear();
												listSolr.addAll(setSolr);
												System.out.println(listSolr);

												Collections.sort(listAPI);
												Collections.sort(listSolr);
												Assert.assertEquals(listAPI, listSolr);
												test.log(LogStatus.INFO, "Brand List From API : " + "\n" + listAPI);
												test.log(LogStatus.INFO, "Brand List From Solr : " + "\n" + listSolr);
												test.log(LogStatus.PASS,
														"Brand Details of API and Solr are matched. Hence the Validation is Passed!!");
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Test case Failed   :  " + e.getMessage());
			System.out.println("Data didnt matched");
			test.log(LogStatus.INFO, "Brand List From API : " + "\n" + listAPI);
			test.log(LogStatus.INFO, "Brand List From Solr : " + "\n" + listSolr);
			test.log(LogStatus.FAIL, "Brand Details of API and Solr Didn't matched. Hence the Validation Failed!!");
		}
	}

	public void categoriesValidation(String endPoint, Logger log, ExtentTest test) throws Throwable {

		String date = null;
		int totalBuyersAPI = 0;
		int totalBuyersSolr = 0;
		int categoryBuyersAPI = 0;
		int categoryBuyersSolr = 0;
		try
		{
			ArrayList<ArrayList<String>> mainExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "main");

			ArrayList<String> clientNameList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client name");

			for (int i = 0; i < clientNameList.size(); i++) { 

				readExcel(endPoint, log, test, "Categories", i);
				ArrayList<String> requestList = excelComputation.returnArayListOfRequestForLocalValues(localValueList, endPointSolr);
				String EndPointSolr = requestList.toString().replaceAll("[\\[\\]]", "");
				System.out.println("EndPointSolr    " + EndPointSolr);
				String responseSolr = NetworkUtils.getCall(EndPointSolr);
				JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);

				ArrayList<ArrayList<String>> apiNameExcelList = readExcel.convertExcelIntoArrayList(type, apiName);
				response = excelComputation.hitApi(apiNameExcelList, request, endPoint,
						log, testCase, localValueList, valueFile);
				JsonElement element = JsonUtils.convertToJsonElement(response);

				JsonArray arr = element.getAsJsonObject().get("data").getAsJsonObject()
						.get("campaign").getAsJsonObject().get("categories")
						.getAsJsonArray();

				List<String> dateListSolr = new ArrayList<String>();

				JsonArray arrSolr = elementSolr.getAsJsonObject().get("response").getAsJsonObject().get("docs").getAsJsonArray();
				arrSolr.iterator().forEachRemaining(p -> dateListSolr.add(p.getAsJsonObject().get("load_date").getAsString()));
				Set<String> s = new LinkedHashSet<String>(dateListSolr);
				dateListSolr.clear();
				dateListSolr.addAll(s);

				for (int k = 0; k < dateListSolr.size(); k++) {
					totalBuyersAPI = 0;
					totalBuyersSolr = 0;
					categoryBuyersAPI = 0;
					categoryBuyersSolr = 0;

					date = dateListSolr.get(k).split("T")[0];
					String date1;

					for (int n = 0; n < arr.size(); n++) {
						date1 = arr.get(n).getAsJsonObject().get("date").getAsString()
								.split("T")[0];

						if (date.contains(date1)) {
							System.out.println("date " + date);
							totalBuyersAPI = Integer.parseInt(arr.get(n).getAsJsonObject().get("totalBuyers").getAsString());
							categoryBuyersAPI = Integer.parseInt(arr.get(n).getAsJsonObject().get("categoryBuyers").getAsString());

							for (int n1 = 0; n1 < arrSolr.size(); n1++) {
								date1 = arrSolr.get(n1).getAsJsonObject()
										.get("load_date").getAsString().split("T")[0];

								if (date.contains(date1)) {
									totalBuyersSolr = totalBuyersSolr + Integer.parseInt(arrSolr.get(n1).getAsJsonObject().get("total_buyers").getAsString());
									categoryBuyersSolr = categoryBuyersSolr + Integer.parseInt(arrSolr.get(n1).getAsJsonObject().get("existing_category_buyer").getAsString());
								}
							}
							System.out.println("totalBuyersAPI for "+ clientNameList.get(i) + totalBuyersAPI);
							System.out.println("totalBuyersSolr for "+ clientNameList.get(i) + totalBuyersSolr);
							System.out.println("categoryBuyersAPI for "+ clientNameList.get(i) + categoryBuyersAPI);
							System.out.println("categoryBuyersSolr for "+ clientNameList.get(i) + categoryBuyersSolr);
							Assert.assertEquals(totalBuyersAPI, totalBuyersSolr);
							Assert.assertEquals(categoryBuyersAPI, categoryBuyersSolr);
							test.log(LogStatus.INFO, "Date : " + "\n" + date);
							test.log(LogStatus.INFO, "totalBuyersAPI for "+ clientNameList.get(i)  + "\n" + totalBuyersAPI);
							test.log(LogStatus.INFO, "totalBuyersSolr for "+ clientNameList.get(i) + "\n" + totalBuyersSolr);
							test.log(LogStatus.INFO, "categoryBuyersAPI for "+ clientNameList.get(i) + "\n" + categoryBuyersAPI);
							test.log(LogStatus.INFO, "categoryBuyersSolr for "+ clientNameList.get(i) + "\n" + categoryBuyersSolr);
							test.log(LogStatus.PASS, "TotalBuyers and Category Buyers of API and Solr matched with respective dates. Hence the Validation Passed!! for "+ clientNameList.get(i));
						}
					}
				}
			} 
		}
		catch(NullPointerException ne)
		{
			System.out.println("No  data found");
		}
		catch (Exception e) {
			Assert.fail("Test case Failed   :  " + e.getMessage());
			System.out.println("Data didnt matched");
			test.log(LogStatus.INFO, "Date : " + "\n" + date);
			test.log(LogStatus.INFO, "totalBuyersAPI : " + "\n" + totalBuyersAPI);
			test.log(LogStatus.INFO, "totalBuyersSolr : " + "\n" + totalBuyersSolr);
			test.log(LogStatus.INFO, "categoryBuyersAPI : " + "\n" + categoryBuyersAPI);
			test.log(LogStatus.INFO, "categoryBuyersSolr : " + "\n" + categoryBuyersSolr);
			test.log(LogStatus.FAIL,
					"TotalBuyers and Category Buyers of API and Solr didn't matched with respective dates. Hence the Validation Failed!!");
		}
	}

	public void RecencyValidation(String endPoint, Logger log, ExtentTest test) throws Throwable {
		String date = null;
		List<String> mobileAppDataListAPI = null;
		List<String> mobileAppDataListSolr = null;
		List<String> campaignTotalDataListAPI = null;
		List<String> campaignTotalDataListSolr = null;
		List<String> mobileWebDataListAPI = null;
		List<String> mobileWebDataListSolr = null;
		List<String> desktopDataListAPI = null;
		List<String> desktopDataListSolr = null;
		try {

			ArrayList<ArrayList<String>> mainExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "main");

			ArrayList<String> clientNameList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client name");

			for (int i = 0; i < clientNameList.size(); i++) {
				readExcel(endPoint, log, test, "Recency", i);
				ArrayList<String> requestList = excelComputation.returnArayListOfRequestForLocalValues(localValueList, endPointSolr);
				String EndPointSolr = requestList.toString().replaceAll("[\\[\\]]", "");
				System.out.println("EndPointSolr    " + EndPointSolr);
				String responseSolr = NetworkUtils.getCall(EndPointSolr);
				JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);

				ArrayList<ArrayList<String>> apiNameExcelList = readExcel
						.convertExcelIntoArrayList(type, apiName);
				response = excelComputation.hitApi(apiNameExcelList, request, endPoint,
						log, testCase, localValueList, valueFile);
				JsonElement element = JsonUtils.convertToJsonElement(response);

				JsonArray arr = element.getAsJsonObject().get("data").getAsJsonObject()
						.get("campaign").getAsJsonObject().get("recency")
						.getAsJsonArray();

				List<String> dateListSolr = new ArrayList<String>();
				List<String> channelTypeListSolr = new ArrayList<String>();
				List<String> channelTypeListAPI = new ArrayList<String>();

				JsonArray arrSolr = elementSolr.getAsJsonObject().get("response")
						.getAsJsonObject().get("docs").getAsJsonArray();
				arrSolr.iterator().forEachRemaining(p -> dateListSolr
						.add(p.getAsJsonObject().get("load_date").getAsString()));
				Set<String> s = new LinkedHashSet<String>(dateListSolr);
				dateListSolr.clear();
				dateListSolr.addAll(s);

				for (int n = 0; n < arr.size(); n++) {
					String date1;
					date1 = arr.get(n).getAsJsonObject().get("date").getAsString()
							.split("T")[0];
					arrSolr.iterator().forEachRemaining(p -> channelTypeListSolr.add(
							p.getAsJsonObject().get("channel_type").getAsString()));
					System.out.println("date " + date1);

					for (int n1 = 0; n1 < channelTypeListSolr.size(); n1++) {
						mobileAppDataListSolr = new ArrayList<String>();
						campaignTotalDataListSolr = new ArrayList<String>();
						mobileWebDataListSolr = new ArrayList<String>();
						desktopDataListSolr = new ArrayList<String>();

						date = arrSolr.get(n1).getAsJsonObject().get("load_date")
								.getAsString().split("T")[0];

						if (channelTypeListSolr.get(n1).contains("mobile-moinapp")) {

							if (date.contains(date1)) {
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("decay").getAsString());
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("normalized").getAsString());
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("expected_revenue").getAsString());
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("buyers").getAsString());
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_unit").getAsString());
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_buyer").getAsString());
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("unit_per_buyer").getAsString());
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("trip_per_buyer").getAsString());
								mobileAppDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_trip").getAsString());

								System.out.println("mobileAppDataListSolr  "+ mobileAppDataListSolr);
							}
						} else if (channelTypeListSolr.get(n1).contains("campaign_total")) {

							if (date.contains(date1)) {
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("decay").getAsString());
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("normalized").getAsString());
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("expected_revenue").getAsString());
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("buyers").getAsString());
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_unit").getAsString());
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_buyer").getAsString());
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("unit_per_buyer").getAsString());
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("trip_per_buyer").getAsString());
								campaignTotalDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_trip").getAsString());

								System.out.println("campaignTotalDataListSolr  "+ campaignTotalDataListSolr);
							}
						} else if (channelTypeListSolr.get(n1).contains("mobile-moweb")) {

							if (date.contains(date1)) {
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("decay").getAsString());
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("normalized").getAsString());
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("expected_revenue").getAsString());
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("buyers").getAsString());
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_unit").getAsString());
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_buyer").getAsString());
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("unit_per_buyer").getAsString());
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("trip_per_buyer").getAsString());
								mobileWebDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_trip").getAsString());

								System.out.println("mobileWebDataListSolr  "+ mobileWebDataListSolr);
							}
						} else if (channelTypeListSolr.get(n1).contains("desktop-de")) {

							if (date.contains(date1)) {
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("decay").getAsString());
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("normalized").getAsString());
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("expected_revenue").getAsString());
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("buyers").getAsString());
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_unit").getAsString());
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_buyer").getAsString());
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("unit_per_buyer").getAsString());
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("trip_per_buyer").getAsString());
								desktopDataListSolr.add(arrSolr.get(n1).getAsJsonObject().get("dol_per_trip").getAsString());

								System.out.println("desktopDataListSolr  " + desktopDataListSolr);
							}
						}
					}
					arr.iterator().forEachRemaining(p -> channelTypeListAPI.add(p.getAsJsonObject().get("channel").getAsString()));

					for (int n2 = 0; n2 < channelTypeListAPI.size(); n2++) {
						mobileAppDataListAPI = new ArrayList<String>();
						campaignTotalDataListAPI = new ArrayList<String>();
						mobileWebDataListAPI = new ArrayList<String>();
						desktopDataListAPI = new ArrayList<String>();

						if (channelTypeListAPI.get(n2).contains("Mobile app")) {
							if (date.contains(date1)) {
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("decay").getAsString());
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("normalized").getAsString());
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("expectedRevenue").getAsString());
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("buyers").getAsString());
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("currencyPerUnit").getAsString());
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("currencyPerBuyer").getAsString());
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("unitPerBuyer").getAsString());
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("tripPerBuyer").getAsString());
								mobileAppDataListAPI.add(arr.get(n2).getAsJsonObject().get("currencyPerTrip").getAsString());

								System.out.println("mobileAppDataListAPI   "+ mobileAppDataListAPI);
							}
						} else if (channelTypeListAPI.get(n2)
								.contains("campaign_total")) {
							if (date.contains(date1)) {
								campaignTotalDataListAPI.add(arr.get(n2).getAsJsonObject().get("decay").getAsString());
								campaignTotalDataListAPI.add(arr.get(n2).getAsJsonObject().get("normalized").getAsString());
								campaignTotalDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("expectedRevenue").getAsString());
								campaignTotalDataListAPI.add(arr.get(n2)
										.getAsJsonObject().get("buyers").getAsString());
								campaignTotalDataListAPI
								.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerUnit").getAsString());
								campaignTotalDataListAPI
								.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerBuyer").getAsString());
								campaignTotalDataListAPI
								.add(arr.get(n2).getAsJsonObject()
										.get("unitPerBuyer").getAsString());
								campaignTotalDataListAPI
								.add(arr.get(n2).getAsJsonObject()
										.get("tripPerBuyer").getAsString());
								campaignTotalDataListAPI
								.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerTrip").getAsString());

								System.out.println("campaignTotalDataListAPI   "
										+ campaignTotalDataListAPI);
							}
						} else if (channelTypeListAPI.get(n2).contains("Mobile web")) {
							if (date.contains(date1)) {
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("decay").getAsString());
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("normalized").getAsString());
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("expectedRevenue").getAsString());
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("buyers").getAsString());
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerUnit").getAsString());
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerBuyer").getAsString());
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("unitPerBuyer").getAsString());
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("tripPerBuyer").getAsString());
								mobileWebDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerTrip").getAsString());

								System.out.println("mobileWebDataListAPI   "
										+ mobileWebDataListAPI);
							}
						} else if (channelTypeListAPI.get(n2).contains("Desktop")) {
							if (date.contains(date1)) {
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("decay").getAsString());
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("normalized").getAsString());
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("expectedRevenue").getAsString());
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("buyers").getAsString());
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerUnit").getAsString());
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerBuyer").getAsString());
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("unitPerBuyer").getAsString());
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("tripPerBuyer").getAsString());
								desktopDataListAPI.add(arr.get(n2).getAsJsonObject()
										.get("currencyPerTrip").getAsString());

								System.out.println(
										"desktopDataListAPI   " + desktopDataListAPI);
							}
						}
					}
					Assert.assertEquals(mobileAppDataListAPI, mobileAppDataListSolr);
					test.log(LogStatus.PASS, "Mobile App Data Validation Passed!! for "+ clientNameList.get(i));
					Assert.assertEquals(campaignTotalDataListAPI,
							campaignTotalDataListSolr);
					test.log(LogStatus.PASS, "Campaign Total Data Validation Passed!! for "+ clientNameList.get(i));
//					Assert.assertEquals(mobileWebDataListAPI, mobileWebDataListSolr);
					test.log(LogStatus.PASS, "Mobile Web Data Validation Passed!! for "+ clientNameList.get(i));
//					Assert.assertEquals(desktopDataListAPI, desktopDataListSolr);
					test.log(LogStatus.PASS, "Desktop Data Validation Passed!! for "+ clientNameList.get(i));

					break;
				}
			} 
		}
		catch(NullPointerException ne)
		{
			System.out.println("No  data found");
		}catch (Exception e) {
			Assert.fail("Test case Failed   :  " + e.getMessage());
			System.out.println("Data didnt matched");
			test.log(LogStatus.FAIL, "Data Validation Failed!!");
		}
	}

	public void channelValidation(String endPoint, Logger log, ExtentTest test) throws Throwable {
		ExcelComputation excelComputation = new ExcelComputation();

		List<String> mobileAppDataListSolrImpression = null;
		List<String> mobileAppDataListSolrEngagements = null;
		List<String> mobileAppDataListSolrBuyers = null;

		List<String> mobileWebDataListSolrImpression = null;
		List<String> mobileWebDataListSolrEngagements = null;
		List<String> mobileWebDataListSolrBuyers = null;

		List<String> desktopDataListSolrImpression = null;
		List<String> desktopDataListSolrEngagements = null;
		List<String> desktopDataListSolrBuyers = null;


		try {

			ArrayList<ArrayList<String>> mainExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "main");

			ArrayList<String> clientNameList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client name");

			for (int i = 0; i < clientNameList.size(); i++) {
				readExcel(endPoint, log, test, "Channel", i);
				ArrayList<String> requestList = excelComputation.returnArayListOfRequestForLocalValues(localValueList, endPointSolr);
				String EndPointSolr = requestList.toString().replaceAll("[\\[\\]]", "");
				System.out.println("EndPointSolr    " + EndPointSolr);
				String responseSolr = NetworkUtils.getCall(EndPointSolr);
				JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);

				ArrayList<ArrayList<String>> apiNameExcelList = readExcel.convertExcelIntoArrayList(type, apiName);
				response = excelComputation.hitApi(apiNameExcelList, request, endPoint,log, testCase, localValueList, valueFile);
				JsonElement element = JsonUtils.convertToJsonElement(response);

				JsonArray arr = element.getAsJsonObject().get("data").getAsJsonObject()
						.get("campaign").getAsJsonObject().get("channels").getAsJsonArray();

				List<String> dateListSolr = new ArrayList<String>();
				List<String> campaignIdListSolr = new ArrayList<String>();
				List<String> channelTypeListAPI = new ArrayList<String>();

				JsonArray arrSolr = elementSolr.getAsJsonObject().get("response")
						.getAsJsonObject().get("docs").getAsJsonArray();
				arrSolr.iterator().forEachRemaining(p -> dateListSolr
						.add(p.getAsJsonObject().get("load_date").getAsString()));
				Set<String> s = new LinkedHashSet<String>(dateListSolr);
				dateListSolr.clear();
				dateListSolr.addAll(s);

				for (int n = 0; n < arr.size(); n++) {

					arrSolr.iterator().forEachRemaining(p -> {
						if (p.getAsJsonObject().get("campaign_id") != null)
							campaignIdListSolr.add(p.getAsJsonObject()
									.get("campaign_id").getAsString());});

					mobileAppDataListSolrImpression = new ArrayList<String>();
					mobileAppDataListSolrEngagements = new ArrayList<String>();
					mobileAppDataListSolrBuyers = new ArrayList<String>();

					mobileWebDataListSolrImpression = new ArrayList<String>();
					mobileWebDataListSolrEngagements = new ArrayList<String>();
					mobileWebDataListSolrBuyers = new ArrayList<String>();

					desktopDataListSolrImpression = new ArrayList<String>();
					desktopDataListSolrEngagements = new ArrayList<String>();
					desktopDataListSolrBuyers = new ArrayList<String>();


					int mobileAppDataSolrImpression = 0;
					int mobileAppDataSolrEngagements = 0;
					int mobileAppDataSolrBuyers = 0;

					int mobileWebDataSolrImpression = 0;
					int mobileWebDataSolrEngagements = 0;
					int mobileWebDataSolrBuyers = 0;

					int desktopDataSolrImpression = 0;
					int desktopDataSolrEngagements = 0;
					int desktopDataSolrBuyers = 0;

					int mobileAppDataAPIImpression = 0;
					int mobileAppDataAPIEngagements = 0;
					int mobileAppDataAPIBuyers = 0;

					int mobileWebDataAPIImpression = 0;
					int mobileWebDataAPIEngagements = 0;
					int mobileWebDataAPIBuyers = 0;

					int desktopDataAPIImpression = 0;
					int desktopDataAPIEngagements = 0;
					int desktopDataAPIBuyers = 0;

					System.out.println(campaignIdListSolr.size());

					for (int d = 0; d < campaignIdListSolr.size(); d++) {


						if (arrSolr.get(d).getAsJsonObject().get("channelscreentype") != null && arrSolr.get(d).getAsJsonObject().get("channelscreentype").getAsString().equals("mobile-moinapp")) {

							mobileAppDataListSolrImpression.add(arrSolr.get(d).getAsJsonObject().get("impressions").getAsString());
							mobileAppDataListSolrEngagements.add(arrSolr.get(d).getAsJsonObject().get("engagements").getAsString());
							mobileAppDataListSolrBuyers.add(arrSolr.get(d).getAsJsonObject().get("buyers").getAsString());

						} else if (arrSolr.get(d).getAsJsonObject().get("channelscreentype") != null && arrSolr.get(d).getAsJsonObject().get("channelscreentype").getAsString().equals("mobile-moweb")) {

							mobileWebDataListSolrImpression.add(arrSolr.get(d).getAsJsonObject().get("impressions").getAsString());
							mobileWebDataListSolrEngagements.add(arrSolr.get(d).getAsJsonObject().get("engagements").getAsString());
							mobileWebDataListSolrBuyers.add(arrSolr.get(d).getAsJsonObject().get("buyers").getAsString());

						} else if (arrSolr.get(d).getAsJsonObject().get("channelscreentype") != null && arrSolr.get(d).getAsJsonObject().get("channelscreentype").getAsString().equals("desktop-de")) {

							desktopDataListSolrImpression.add(arrSolr.get(d).getAsJsonObject().get("impressions").getAsString());
							desktopDataListSolrEngagements.add(arrSolr.get(d).getAsJsonObject().get("engagements").getAsString());
							desktopDataListSolrBuyers.add(arrSolr.get(d).getAsJsonObject().get("buyers").getAsString());

						}
					}
					/////////////////////////////////////////////////////////////////////////
					for(int a=0; a<mobileAppDataListSolrImpression.size(); a++)
					{
						mobileAppDataSolrImpression = mobileAppDataSolrImpression + Integer.parseInt(mobileAppDataListSolrImpression.get(a));
					}
					System.out.println("mobileAppDataSolrImpression  "+mobileAppDataSolrImpression);

					for(int a=0; a<mobileWebDataListSolrImpression.size(); a++)
					{
						mobileWebDataSolrImpression = mobileWebDataSolrImpression + Integer.parseInt(mobileWebDataListSolrImpression.get(a));
					}
					System.out.println("mobileWebDataSolrImpression  "+mobileWebDataSolrImpression);

					for(int a=0; a<desktopDataListSolrImpression.size(); a++)
					{
						desktopDataSolrImpression = desktopDataSolrImpression + Integer.parseInt(desktopDataListSolrImpression.get(a));
					}
					System.out.println("desktopDataSolrImpression  "+desktopDataSolrImpression);

					/////////////////////////////////////////////////////////////////////////

					for(int a=0; a<mobileAppDataListSolrEngagements.size(); a++)
					{
						mobileAppDataSolrEngagements = mobileAppDataSolrEngagements + Integer.parseInt(mobileAppDataListSolrEngagements.get(a));
					}
					System.out.println("mobileAppDataSolrEngagements  "+mobileAppDataSolrEngagements);

					for(int a=0; a<mobileWebDataListSolrEngagements.size(); a++)
					{
						mobileWebDataSolrEngagements = mobileWebDataSolrEngagements + Integer.parseInt(mobileWebDataListSolrEngagements.get(a));
					}
					System.out.println("mobileWebDataSolrEngagements  "+mobileWebDataSolrEngagements);

					for(int a=0; a<desktopDataListSolrEngagements.size(); a++)
					{
						desktopDataSolrEngagements = desktopDataSolrEngagements + Integer.parseInt(desktopDataListSolrEngagements.get(a));
					}
					System.out.println("desktopDataSolrEngagements  "+desktopDataSolrEngagements);

					/////////////////////////////////////////////////////////////////////////

					for(int a=0; a<mobileAppDataListSolrBuyers.size(); a++)
					{
						mobileAppDataSolrBuyers = mobileAppDataSolrBuyers + Integer.parseInt(mobileAppDataListSolrBuyers.get(a));
					}
					System.out.println("mobileAppDataSolrBuyers  "+mobileAppDataSolrBuyers);

					for(int a=0; a<mobileWebDataListSolrBuyers.size(); a++)
					{
						mobileWebDataSolrBuyers = mobileWebDataSolrBuyers + Integer.parseInt(mobileWebDataListSolrBuyers.get(a));
					}
					System.out.println("mobileWebDataSolrBuyers  "+mobileWebDataSolrBuyers);

					for(int a=0; a<desktopDataListSolrBuyers.size(); a++)
					{
						desktopDataSolrBuyers = desktopDataSolrBuyers + Integer.parseInt(desktopDataListSolrBuyers.get(a));
					}
					System.out.println("desktopDataSolrBuyers  "+desktopDataSolrBuyers);

					/////////////////////////////////////////////////////////////////////////


					arr.iterator().forEachRemaining(p -> {
						if (p.getAsJsonObject().get("name") != null && !p.getAsJsonObject().get("name").isJsonNull())
							channelTypeListAPI.add(p.getAsJsonObject().get("name").getAsString());});


					for (int n2 = 0; n2 < channelTypeListAPI.size(); n2++) {

						if (channelTypeListAPI.get(n2).contains("Mobile app")) {
							mobileAppDataAPIImpression = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("impressions").getAsString());
							mobileAppDataAPIEngagements = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("engagements").getAsString());
							mobileAppDataAPIBuyers = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("buyers").getAsString());

						} else if (channelTypeListAPI.get(n2).contains("Mobile web")) {
							mobileWebDataAPIImpression = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("impressions").getAsString());
							mobileWebDataAPIEngagements = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("engagements").getAsString());
							mobileWebDataAPIBuyers = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("buyers").getAsString());

						} else if (channelTypeListAPI.get(n2).contains("Desktop")) {
							desktopDataAPIImpression = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("impressions").getAsString());
							desktopDataAPIEngagements = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("engagements").getAsString());
							desktopDataAPIBuyers = Integer.parseInt(arr.get(n2).getAsJsonObject().get("performance").getAsJsonObject().get("buyers").getAsString());

						}
					}
					Assert.assertEquals(mobileAppDataAPIEngagements, mobileAppDataSolrEngagements);
					Assert.assertEquals(mobileAppDataAPIBuyers, mobileAppDataSolrBuyers);
					test.log(LogStatus.PASS, "Mobile App Data Validation Passed!!");
					Assert.assertEquals(mobileWebDataAPIEngagements, mobileWebDataSolrEngagements);
					Assert.assertEquals(mobileWebDataAPIBuyers, mobileWebDataSolrBuyers);
					test.log(LogStatus.PASS, "Mobile Web Data Validation Passed!!");
					Assert.assertEquals(desktopDataAPIEngagements, desktopDataSolrEngagements);
					Assert.assertEquals(desktopDataAPIBuyers, desktopDataSolrBuyers);
					test.log(LogStatus.PASS, "Desktop Data Validation Passed!!");

					break;
				}
			}
		} 
		catch(NullPointerException ne)
		{
			System.out.println("No  data found");
		}catch (Exception e) {
			Assert.fail("Test case Failed   :  " + e.getMessage());
			System.out.println("Data didnt matched");
			test.log(LogStatus.FAIL, "Data Validation Failed!!");
		}
	}

	public void trialAndRepeatValidation(String endPoint, Logger log, ExtentTest test) throws Throwable {
		try
		{
			//			readExcel(endPoint, log, test, "UPCDetails");
			System.out.println("EndPointSolr    " + endPointSolr);
			String responseSolr = NetworkUtils.getCall(endPointSolr);
			JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);

			ArrayList<ArrayList<String>> apiNameExcelList = readExcel.convertExcelIntoArrayList(type, apiName);
			response = excelComputation.hitApi(apiNameExcelList, request, endPoint,
					log, testCase, localValueList, valueFile);
			JsonElement element = JsonUtils.convertToJsonElement(response);
			JsonArray arr = element.getAsJsonObject().get("data").getAsJsonObject()
					.get("campaign").getAsJsonObject().get("upcList").getAsJsonArray();
			arr.iterator().forEachRemaining(
					p -> listAPI.add(p.getAsJsonObject().get("upc").getAsString()));
			Set<String> set = new HashSet<String>(listAPI);
			listAPI.clear();
			listAPI.addAll(set);
			JsonArray arrSolr = elementSolr.getAsJsonObject().get("response")
					.getAsJsonObject().get("docs").getAsJsonArray();
			arrSolr.iterator().forEachRemaining(p -> listSolr.add(p.getAsJsonObject().get("upc").getAsString()));
			Set<String> setSolr = new HashSet<String>(listSolr);
			listSolr.clear();
			listSolr.addAll(setSolr);
			System.out.println(listSolr);

			Collections.sort(listAPI);
			Collections.sort(listSolr);
			Assert.assertEquals(listAPI, listSolr);
			test.log(LogStatus.INFO, "UPC List From API : " + "\n" + listAPI);
			test.log(LogStatus.INFO, "UPC List From Solr : " + "\n" + listSolr);
			test.log(LogStatus.PASS, "UPC Details of API and Solr are matched. Hence the Validation is Passed!!");
		} catch (Exception e) {
			Assert.fail("Test case Failed   :  " + e.getMessage());
			System.out.println("Data didnt matched");
			test.log(LogStatus.INFO, "UPC List From API : " + "\n" + listAPI);
			test.log(LogStatus.INFO, "UPC List From Solr : " + "\n" + listSolr);
			test.log(LogStatus.FAIL, "UPC Details of API and Solr Didn't matched. Hence the Validation Failed!!");
		}
	}

	public void creativeValidation(String endPoint, Logger log, ExtentTest test) throws Throwable {
		try {
			String creativeid;
			List<String> solrImpression = null;
			List<String> solrBuyers = null;

			ArrayList<ArrayList<String>> mainExcelArrayList = readExcel.convertExcelIntoArrayList("mainExcel", "main");

			ArrayList<String> clientNameList = readExcel.returnColoumnOfTheExcel(mainExcelArrayList, "Client name");

			for (int i = 0; i < clientNameList.size(); i++) {
				readExcel(endPoint, log, test, "Creative", i);
				endPointSolr = endPointSolr.replace("CAMPAIGNID", campaignId);
				System.out.println("EndPointSolr    " + endPointSolr);
				String responseSolr = NetworkUtils.getCall(endPointSolr);
				JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);

				ArrayList<ArrayList<String>> apiNameExcelList = readExcel.convertExcelIntoArrayList(type, apiName);
				ApiOperation apiOperation = new ApiOperation();
				request = request.replace("2020-01-01", "2019-09-30").replace("2020-05-31", "2019-10-27");
				response = apiOperation.POSTRequest(endPoint, request, "General", log, testCase);
				JsonElement element = JsonUtils.convertToJsonElement(response);

				JsonArray arr = element.getAsJsonObject().get("data").getAsJsonObject()
						.get("campaign").getAsJsonObject().get("creatives")
						.getAsJsonArray();

				List<String> creativeIdListSolr = new ArrayList<String>();

				JsonArray arrSolr = elementSolr.getAsJsonObject().get("response").getAsJsonObject().get("docs").getAsJsonArray();
				arrSolr.iterator().forEachRemaining(p -> creativeIdListSolr.add(p.getAsJsonObject().get("creative_id").getAsString()));
				Set<String> s = new LinkedHashSet<String>(creativeIdListSolr);
				creativeIdListSolr.clear();
				creativeIdListSolr.addAll(s);

				System.out.println("CreativeIdListSolr   "+creativeIdListSolr);

				List<String> creativeIdListAPI = new ArrayList<String>();

				arr.iterator().forEachRemaining(p -> creativeIdListAPI.add(p.getAsJsonObject().get("id").getAsString()));
				Set<String> s1 = new LinkedHashSet<String>(creativeIdListAPI);
				creativeIdListAPI.clear();
				creativeIdListAPI.addAll(s1);

				System.out.println("CreativeIdListAPI   "+creativeIdListAPI);
				int slrImpressionApi = 0;
				int slrBuyersApi = 0;
				for (int n = 0; n < creativeIdListAPI.size(); n++) {

					creativeid = creativeIdListAPI.get(n);
					System.out.println("CreativeId   "+creativeid);
					System.out.println(arr.get(n).getAsJsonObject().get("id").getAsString());
					if(creativeid.contains(arr.get(n).getAsJsonObject().get("id").getAsString()))
					{
						slrImpressionApi = Integer.parseInt(arr.get(n).getAsJsonObject().get("performance").getAsJsonObject().get("impressions").getAsString());
						slrBuyersApi = Integer.parseInt(arr.get(n).getAsJsonObject().get("performance").getAsJsonObject().get("buyers").getAsString());
					}
					solrImpression = new ArrayList<String>();
					solrBuyers = new ArrayList<String>();

					int slrImpression = 0;
					int slrBuyers = 0;

					Map<String, Integer> counts = new HashMap<String, Integer>();

					for (int d = 0; d < arrSolr.size(); d++) {

						if (creativeid.contains(arrSolr.get(d).getAsJsonObject().get("creative_id").getAsString()))
						{
							solrBuyers.add(arrSolr.get(d).getAsJsonObject().get("buyers").getAsString());
							counts.put(arrSolr.get(d).getAsJsonObject().get("load_date").getAsString(), 
									Integer.parseInt(arrSolr.get(d).getAsJsonObject().get("impression_count").getAsString()));
						}
					}
						System.out.println("abcd  "+counts);
					
						String[] count = counts.toString().split(",");
						for(int t=0; t<count.length; t++)
						{
							solrImpression.add(count[t].split("=")[1].replace("}", ""));
						}
					System.out.println("Impression   "+solrImpression);
					System.out.println("Buyers   "+solrBuyers);
					for(int a=0; a<solrImpression.size(); a++)
					{
						slrImpression = slrImpression + Integer.parseInt(solrImpression.get(a));
					}
					System.out.println("slrImpression  "+slrImpression);

					for(int a=0; a<solrBuyers.size(); a++)
					{
						slrBuyers = slrBuyers + Integer.parseInt(solrBuyers.get(a));
					}
					System.out.println("slrBuyers  "+slrBuyers);

					Assert.assertEquals(slrImpressionApi, slrImpression);
					Assert.assertEquals(slrBuyersApi, slrBuyers);

					break;
				}
				break;
			}
			ReadPropertyFile readPropertyFile = new ReadPropertyFile();
			String EndPointSolr = readPropertyFile.returnValue("EndPointCreativeTab");
			System.out.println("EndPointSolr    " + EndPointSolr);
			String responseSolr = NetworkUtils.getCall(EndPointSolr);
			JsonElement elementSolr = JsonUtils.convertToJsonElement(responseSolr);
			JsonArray campaignSolr = elementSolr.getAsJsonObject().get("facet_counts").getAsJsonObject().get("facet_fields").getAsJsonObject().get("campaign_id").getAsJsonArray();
			for(int m =0; m <campaignSolr.size(); m++)
			{
				if(campaignSolr.get(m).getAsString().length()>5)
				{
					ArrayList<ArrayList<String>> apiNameExcelList = readExcel.convertExcelIntoArrayList(type, apiName);
					request = readExcel.readTextFile("postRequestBodyCampaignData");
					request = request.replace("CAMPAIGNID", campaignSolr.get(m).getAsString());
					ApiOperation apiOperation = new ApiOperation();
					response = apiOperation.POSTRequest(endPoint, request, "General", log, testCase);
					System.out.println("Response   "+response);
					JsonElement element = JsonUtils.convertToJsonElement(response);
					String message = element.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").getAsString();
					System.out.println("Message  "+message);
					Assert.assertEquals(message, "API_DATA_LIMIT_ERROR");
				}
				break;
			}
			test.log(LogStatus.PASS, "Creative Validation is Passed!!");
		}catch(NullPointerException ne)
		{
			test.log(LogStatus.FAIL, "Creative Validation Failed!!");
			System.out.println("No  data found");
		}

	}
}