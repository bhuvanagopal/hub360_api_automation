package com.catalina.hub360.steps;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.catalina.hub360.utils.ExtentConfig;
import com.catalina.hub360.utils.ReadPropertyFile;




public class CreativeValidation extends ExtentConfig {
	private static final Logger log = Logger.getLogger(CreativeValidation.class);
	Verification verification;
	
	@Test
	public void Validation_CreativeValidation() throws Throwable {
		test = report.startTest("Validation_CreativeValidation");
		ReadPropertyFile readPropertyFile = new ReadPropertyFile();
		String endPoint = readPropertyFile.returnValue("EndPoint");
		try
		{
			verification = new Verification();
			verification.creativeValidation(endPoint, log, test);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}
