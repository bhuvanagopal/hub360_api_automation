package com.catalina.hub360.steps;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.catalina.hub360.utils.ReadPropertyFile;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class CartInSightValidation {
	public static final String ConfigFilePath = "config.properties";
	private static final Logger log = Logger.getLogger(CartInSightValidation.class);
	Verification verification;
	Hub hub = new Hub();
	ExtentReports report;
	ExtentTest test;

	@BeforeTest
	public void startReport() throws IOException {
		report = hub.startTest();
	}

	
	@Test
	public void CartInSightValidation_() throws Throwable {
		test = report.startTest("CartInSightValidation");
		ReadPropertyFile readPropertyFile = new ReadPropertyFile();
		String endPoint = readPropertyFile.returnValue("EndPoint");
		test.log(LogStatus.PASS, "Brand Details of API and Solr are matched. Hence the Validation is Passed!!");
		try
		{
			verification = new Verification();
			verification.cartInSight_DataValidation(endPoint, log, test);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@AfterTest
	public void endReport() {
		hub.endTest();
	}

}
