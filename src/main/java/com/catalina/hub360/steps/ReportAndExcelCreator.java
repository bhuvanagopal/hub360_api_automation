package com.catalina.hub360.steps;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.catalina.hub360.utils.Computation;
import com.catalina.hub360.utils.Creation;
import com.catalina.hub360.utils.Report;
import com.catalina.hub360.utils.TargetProcess;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ReportAndExcelCreator {
	static ExtentReports report;
	static ExtentTest test;
	private static final TargetProcess tp = new TargetProcess();
	
	@BeforeTest
	public ExtentReports startTest() throws IOException {
		report = new ExtentReports("ExtentReportResults.html");
		report.loadConfig(new File("extent-config.xml"));
		return report;	
	}
	
	
	public void createReportExcel() throws IOException {
		ApiOperation apiOperation = new ApiOperation();
		Computation composition = new Computation();
		Creation creation = new Creation();
		
		apiOperation.fetchTestCaseDetails();
		Map<String, ArrayList<Report>> reportRunDetailMap = composition.reportDataProcessing();
		test = creation.createExtentReport(test, report, reportRunDetailMap);

	}
	
	@AfterTest
	public static void endTest() {
		report.endTest(test);
		report.flush();
		tp.uploadAttachment(new File("./ExtentReportResults.html"), tp.getBuildId());
	}
}
