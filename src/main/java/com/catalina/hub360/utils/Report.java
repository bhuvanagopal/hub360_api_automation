package com.catalina.hub360.utils;

public class Report {

String testcaseName;
String clientName;
String responseCode;
String responseMessage;
String responseTime;

public String getTestcaseName() {
	return testcaseName;
}
public void setTestcaseName(String testcaseName) {
	this.testcaseName = testcaseName;
}
public String getClientName() {
	return clientName;
}
public void setClientName(String clientName) {
	this.clientName = clientName;
}
public String getResponseCode() {
	return responseCode;
}
public String getResponseTime() {
	return responseTime;
}
public void setResponseTime(String responseTime) {
	this.responseTime = responseTime;
}
public void setResponseCode(String responseCode) {
	this.responseCode = responseCode;
}
public String getResponseMessage() {
	return responseMessage;
}
public void setResponseMessage(String responseMessage) {
	this.responseMessage = responseMessage;
}
@Override
public String toString() {
	return "Report [testcaseName=" + testcaseName + ", clientName=" + clientName + ", responseCode=" + responseCode
			+ ", responseMessage=" + responseMessage + "]";
}




}
