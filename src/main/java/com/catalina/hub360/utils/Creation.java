package com.catalina.hub360.utils;

import java.util.ArrayList;
import java.util.Map;

import com.catalina.hub360.utils.Report;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Creation {

	public ExtentTest createExtentReport(ExtentTest test, ExtentReports report,
			Map<String, ArrayList<Report>> reportRunDetailMap) {

		for (Map.Entry<String, ArrayList<Report>> entry : reportRunDetailMap.entrySet()) {
			test = report.startTest(entry.getKey());
			StringBuilder str = new StringBuilder(
					"<table><tr><th style=\"width:20%\">Client</th><th style=\"width:20%\">Status Code</th><th style=\"width:20%\">Response Message</th><th style=\"width:20%\">Status</th><th style=\"width:20%\">Response Time</th></tr>");
			LogStatus status=LogStatus.PASS;
			for (Report data : entry.getValue()) {
				str.append("<tr>");
				str.append("<td>" + data.getClientName() + "</td>");
				
				str.append("<td>" + data.getResponseCode() + "</td>");
				if(status==LogStatus.PASS && (!data.getResponseCode().equals("200"))) {
					status=LogStatus.FAIL;
				}
				
				
				str.append("<td>" + data.getResponseMessage() + "</td>");
				if(data.getResponseMessage().equalsIgnoreCase("OK")) {str.append("<td> <span class=\"btn-floating btn-small waves-effect waves-light green\" status=\"pass\" alt=\"pass\" title=\"pass\"><i class=\"mdi-action-check-circle\"></i></span></td>");
				}else { str.append("<td> <span class=\"btn-floating btn-small waves-effect waves-light red\" status=\"fail\" alt=\"fail\" title=\"fail\"><i class=\"mdi-navigation-cancel\"></i></span> </td>");
}
				str.append("<td>" + data.getResponseTime() + "</td>");
				str.append("</tr>");
			}

			str.append("</table>");
			
			test.log(status, str.toString());

		}

		return test;
	}
}
