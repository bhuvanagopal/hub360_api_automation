package com.catalina.hub360.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;

public class ReadPropertyFile {
	
	
 private static String fileLocation=System.getProperty("user.dir")+"\\config.properties";
	public String returnValue(String searchKey) throws IOException {
		String value="";
		Properties prop = new Properties();
		FileInputStream ip= new FileInputStream(fileLocation);
		prop.load(ip);
		value=prop.getProperty(searchKey);
		
		return value;
	}
	
	public void configureLog4j() {
		 File file = new File(".\\Logs\\application.log"); 
         
	        if(file.delete()) 
	          System.out.println("File deleted successfully"); 
	         
	        else
	         System.out.println("Failed to delete the file"); 
	       
		PropertyConfigurator.configure("log4j.properties");
		
	}
	
	public String getBuildName() throws IOException {
		System.out.println("Inside getBuildName");
		String value="";
		Properties prop = new Properties();
		if (System.getProperty("BuildName") == null) {
			FileInputStream ip= new FileInputStream(fileLocation);
			prop.load(ip);
			value=prop.getProperty("BuildName");
			return value;
		} else {
			return System.getProperty("BuildName");
		}

	}

	public String getTestPlanID() throws IOException {
		String value="";
		Properties prop = new Properties();
		if (System.getProperty("TestPlanID") == null) {
			FileInputStream ip= new FileInputStream(fileLocation);
			prop.load(ip);
			value=prop.getProperty("TestPlanId");
			return value;
		} else {
			return System.getProperty("TestPlanID");
		}
	}
}
