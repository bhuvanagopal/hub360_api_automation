package com.catalina.hub360.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.catalina.hub360.utils.Report;
import com.catalina.hub360.utils.TestCase;

public class Computation {
	static  ArrayList<TestCase> testRunDetailList = new ArrayList<TestCase>();

	Map<String, ArrayList<Report>> reportRunDetailMap = new HashMap<String, ArrayList<Report>>();

	public void initializeTestRunDetails(ArrayList<TestCase> testCaseDetailList) {
		testRunDetailList=testCaseDetailList;
	}

	public Map<String, ArrayList<Report>> reportDataProcessing() {
		Set<String> apiSet= returnListOfApi();


		for(String apiName: apiSet) {
			ArrayList<Integer> indexList= returnIndexList(apiName);
			ArrayList<Report> reportRunDetailList = new ArrayList<Report>();
			String apiType="";

			for(int i=0;i<indexList.size();i++) {

				int index=indexList.get(i);
				Report report = new Report();

				apiType=testRunDetailList.get(index).getApiType();
				report.setTestcaseName(apiName);
				report.setClientName(testRunDetailList.get(index).getClientName());
				report.setResponseCode(testRunDetailList.get(index).getResponseCode());
				report.setResponseMessage(testRunDetailList.get(index).getResponseMessage());
				report.setResponseTime(testRunDetailList.get(index).getResponseTime());
				reportRunDetailList.add(report);

			}
			reportRunDetailMap.put(apiType+"_"+apiName, reportRunDetailList);
		}

		return reportRunDetailMap;
	}

	public Set<String> returnListOfApi() {
		Set<String> apiSet = new HashSet<>();

		for(int i=0;i< testRunDetailList.size();i++) {
			TestCase testCase =testRunDetailList.get(i);
			apiSet.add(testCase.getApiName());
		}
		return apiSet;
	}

	public ArrayList<Integer> returnIndexList(String apiName){
		ArrayList<Integer> indexList = new ArrayList<Integer>();

		for(int i=0;i< testRunDetailList.size();i++) {
			TestCase testCase =testRunDetailList.get(i);
			if(apiName.equalsIgnoreCase(testCase.getApiName())) {
				indexList.add(i);	
			}
		}

		return indexList;

	}
}
