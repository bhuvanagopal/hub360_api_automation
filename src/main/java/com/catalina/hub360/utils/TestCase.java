package com.catalina.hub360.utils;

public class TestCase {
String clientName;
String clientId;
String listId;
String apiType;
String apiName;
String request;
String response;
String responseCode;
String responseMessage;
String responseTime;

public String getResponseTime() {
	return responseTime;
}
public void setResponseTime(String responseTime) {
	this.responseTime = responseTime;
}
public String getResponseMessage() {
	return responseMessage;
}
public void setResponseMessage(String responseMessage) {
	this.responseMessage = responseMessage;
}
public String getClientName() {
	return clientName;
}
public void setClientName(String clientName) {
	this.clientName = clientName;
}
public String getClientId() {
	return clientId;
}
public void setClientId(String clientId) {
	this.clientId = clientId;
}
public String getListId() {
	return listId;
}
public void setListId(String listId) {
	this.listId = listId;
}
public String getApiType() {
	return apiType;
}
public void setApiType(String apiType) {
	this.apiType = apiType;
}
public String getApiName() {
	return apiName;
}
public void setApiName(String apiName) {
	this.apiName = apiName;
}
public String getRequest() {
	return request;
}
public void setRequest(String request) {
	this.request = request;
}
public String getResponse() {
	return response;
}
public void setResponse(String response) {
	this.response = response;
}
public String getResponseCode() {
	return responseCode;
}
public void setResponseCode(String responseCode) {
	this.responseCode = responseCode;
}
@Override
public String toString() {
	return "TestCase [clientName=" + clientName + ", clientId=" + clientId + ", listId=" + listId + ", apiType="
			+ apiType + ", apiName=" + apiName + ", request=" + request + ", response=" + response + ", responseCode="
			+ responseCode + "]";
}

}
