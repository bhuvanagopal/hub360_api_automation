package com.catalina.hub360.utils;


import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.hub360.steps.Hub;
import com.cucumber.listener.Reporter;
import com.google.common.io.Files;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import entities.TestCaseRun;

public class SharedResource {

	WebDriver driver;
	Scenario scenario;
	TargetProcess targetProcess;
	Hub hub;

	public WebDriver init(){
		if(driver==null){
			System.setProperty("webdriver.driver.chrome", "./chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}
		return driver;
	}

	@Before
	public void beforeHook(Scenario scenario) throws IOException {
		this.scenario = scenario;
		System.out.println("Scenario Name--"+scenario.getName());
	}

	@After
	public void afterHook(Scenario scenario) throws Throwable {
		System.out.println("Completed the Scenario--"+scenario.getName());
		addingScreenShot(scenario);
		Reporter.addScenarioLog("Finished Scinario : " + scenario.getName());
		System.out.println("Scenario status :- "+ scenario.getStatus());
		String name = scenario.getName();
		if(name.contains("TestCaseIds"))
		{
			String[] list  = name.split("TestCaseIds");
			String[] list1  = list[1].split(",");
			int[] testcaseId = Arrays.asList(list1).stream().map(String::trim).mapToInt(Integer::parseInt).toArray();
			TargetProcess tp= new TargetProcess();
			try{

				String status = this.scenario.getStatus();
				TestCaseRun testCaseRun = new TestCaseRun(status,"Automation Update");
				for(int i=0; i<testcaseId.length; i++)
				{
//					if(testRunner.buildID!=-1)
//					{
//						tp.updateStatus(hub.buildID, testcaseId[i], testCaseRun);
//					}
				}
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("Exception : In Method: UpdateTCResult() "+e.getMessage());
			}
		}
		else
		{
			System.out.println("No id's Present");
		}
		driver.close();	
	}



	public Scenario getScenario() {
		return scenario;
	}

	private void addingScreenShot(Scenario scenario) throws IOException, InterruptedException {
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
		File sourcePath = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);


		File destination = new File(System.getProperty("user.dir")
				+ "/target/screenshots");


		if (!destination.exists()) {
			System.out.println("File created " + destination);
			destination.mkdir();
		}

		File destinationPath = new File(System.getProperty("user.dir") + "/target/screenshots/" + scenario.getName() + ".png");


		Files.copy(sourcePath, destinationPath);   

		//This attach the specified screenshot to the test
		Reporter.addScreenCaptureFromPath(System.getProperty("user.dir")+"/target/screenshots/" + scenario.getName() + ".png");
	}

	public void addScreenshotToScreen(String imageName) throws InterruptedException, IOException {
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS) ;
		File sourcePath = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);

		File destinationPath = new File(System.getProperty("user.dir")
				+ "/target/"
				+ imageName + ".png");
		Files.copy(sourcePath, destinationPath);

		Reporter.addStepLog("<img src=System.getProperty(\"user.dir\")+\"/target/screenshots/"
				+ imageName + ".png"+"\">");
	}
	
	public void waitForLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
                try {
        			WebDriverWait wait = new WebDriverWait(driver, 30000);
        			wait.until(pageLoadCondition);
        		} catch (Throwable error) {
        			Assert.fail("Timeout waiting for Page Load Request to complete.");
        		}
	}
	
}
