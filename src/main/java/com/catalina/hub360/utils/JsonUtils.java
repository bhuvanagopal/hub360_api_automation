package com.catalina.hub360.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class JsonUtils {

	private final static Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private static JsonParser parser = new JsonParser();

	public static JsonElement convertToJsonElement(String data) {
		LOGGER.log(Level.INFO, "Converting data to JsonElement Object :-\n"
				+ parser.parse(data));
		return parser.parse(data);
	}
}
