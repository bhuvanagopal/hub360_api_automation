package com.catalina.hub360.utils;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


public class ExtentConfig {
	protected static ExtentReports report;
	protected static ExtentTest test;

	@BeforeTest
	public static ExtentReports startTest() throws IOException {
		report = new ExtentReports("ExtentReportResults.html");
		report.loadConfig(new File("extent-config.xml"));
		return report;	
	}


	@AfterTest
	public static void endTest() {
		report.endTest(test);
		report.flush();
	}
}
