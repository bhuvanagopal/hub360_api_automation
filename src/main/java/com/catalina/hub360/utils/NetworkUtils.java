
package com.catalina.hub360.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class NetworkUtils {

	private final static Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static String getCall(String url) {

		LOGGER.log(Level.INFO, "Hitting a get call to url :- " + url);
		Client client = ClientBuilder.newClient();
		return client.target(url).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get()
				.readEntity(String.class);
	}

	public static String getCallWithQueryParameter(String url, String... params) {

		LOGGER.log(Level.INFO, "Hitting a get call to url :- " + url);
		Client client = ClientBuilder.newClient();

		WebTarget target = client.target(url);
		for (String param : params) {
			LOGGER.log(Level.INFO, "Adding query Parameter " + param);
			target = target
					.queryParam(param.split("=")[0], param.split("=")[1]);

		}

		LOGGER.log(Level.INFO, "Final request is :- " + target.toString());

		return target.request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get()
				.readEntity(String.class);
	}

	public static String postCall(String url, String data) throws IOException {

		LOGGER.log(Level.INFO, "Hitting a post request to url :- " + url
				+ "\n Data is :- \n" + data);
		URL obj = new URL(url);
		HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
		postConnection.setRequestMethod("POST");

		postConnection.setRequestProperty("Host","hub360-api.catalina.com");
		postConnection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0");
		postConnection.setRequestProperty("Accept","application/json, text/plain, */*");
		postConnection.setRequestProperty("Accept-Language","en-US,en;q=0.5");
		postConnection.setRequestProperty("Accept-Encoding","gzip, deflate, br");
		postConnection.setRequestProperty("Content-Type","application/json");
		postConnection.setRequestProperty("Content-Length","268");
		postConnection.setRequestProperty("Origin","https://hub360.catalina.com");
		postConnection.setRequestProperty("Connection","keep-alive");
		postConnection.setRequestProperty("Referer","https://hub360.catalina.com/login");
		postConnection.setRequestProperty("Sec-Fetch-Site", "same-site");
		postConnection.setRequestProperty("Sec-Fetch-Mode", "cors");
		
		Client client = ClientBuilder.newClient();
		return client.target(url).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(data, MediaType.APPLICATION_JSON))
				.readEntity(String.class);
	}
	
	public static String putCall(String url, String data) {

		LOGGER.log(Level.INFO, "Hitting a put request to url :- " + url
				+ "\n Data is :- \n" + data);
		Client client = ClientBuilder.newClient();
		return client.target(url).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.put(Entity.entity(data, MediaType.APPLICATION_JSON))
				.readEntity(String.class);
	}
	
}
